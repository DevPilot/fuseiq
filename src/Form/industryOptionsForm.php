<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */
namespace Drupal\fuseiq\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Render\Element\Fieldset;

class industryOptionsForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'industry_options_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  
    $form = array();
    $form['industry'] = array(
      '#type' => 'radios',
      '#options' => $this->taxonomy_list('new_taxonomy', 1),
    );  
	
	  

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    
  }

  
  // Function to get a list based on Taxonomy vocab machine name
  public function taxonomy_list($vocab, $all = NULL) {
    $tax_items = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree($vocab);
  
    
    if($all == 1) {
  
      $tax_list_option['*'] = 'Select All';
    	foreach ($tax_items as $tax_item) {
    		$tax_list_option[$tax_item->tid] = $tax_item->name;
    	}
  
      return $tax_list_option;
    } else {
    	foreach ($tax_items as $tax_item) {
    		$tax_list_option[$tax_item->tid] = $tax_item->name;
    	}    
      return $tax_list_option;
    }
    
  }
  
}