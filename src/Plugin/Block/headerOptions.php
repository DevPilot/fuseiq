<?php

namespace Drupal\fuseiq\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
use Drupal\Core\Entity;
/**
 * Provides a 'Header' Block.
 *
 * @Block(
 *   id = "fuseiq_header",
 *   admin_label = @Translation("Header with options for child pages"),
 * )
 */
class headerOptions extends BlockBase {

  /**
   * {@inheritdoc}
   */
     
  public function build() {	  
	  
  	$node = \Drupal::routeMatch()->getParameter('node');
  	$content_type = $node->type->getEntity()->getType();
  	$applied_content_types = array('case_study','article','page');
  	$inherit = 0;
  	switch ($content_type) {
    	case "page":
    	  $default_view_display = 'default_header_embed';
    	  break;
      case "case_study":
        $default_view_display = 'default_case_study_embed';
        break;  
      case "article":
        $default_view_display = 'default_article_embed';
        break;  
      default:
        $default_view_display = 'default_header_embed';

  	}
  	if(in_array($node->type->getEntity()->getType(), $applied_content_types) && $node->hasField('field_header_paragraph')) {
      if($node->hasField('field_inherit_header')) {
    	  $inherit = $node->get('field_inherit_header')->value;
    	}

    	if ((!$node->get('field_header_paragraph')->isEmpty()) || (($inherit != 0))) {
      	$node_id = $node->id();
      	
      	if($inherit == 1) {
          $menu_link_manager = \Drupal::service('plugin.manager.menu.link');
          if ($node_id) {
            $menu_link = $menu_link_manager->loadLinksByRoute('entity.node.canonical', array('node' => $node_id));
          }
          else {
            return '';
          }
          if (is_array($menu_link) && count($menu_link)) {
            $menu_link = reset($menu_link);
            if ($menu_link->getParent()) {
              $parents = $menu_link_manager->getParentIds($menu_link->getParent());
              $reverse = array_reverse($parents);
              $parent = reset($reverse);
              $title = $menu_link_manager->createInstance($parent)->getTitle();
              $source_node = $menu_link_manager->createInstance($parent)->getUrlObject()->getRouteParameters()['node'];
              $view_display = 'inherit_embed';
            }
          }
      	} else {
        	$source_node = $node_id;
        	$view_display = 'header_embed';
      	} 
      	$view = \Drupal\views\Views::getView('header');
        if (is_object($view)) {
          if(!empty($source_node)) {
            $view->setDisplay($view_display);
            $view->preExecute();
            $view->setArguments(array($source_node));
            $view->execute();
            $content = $view->buildRenderable();
            $output[] = $content;
          } else {
            $view->setDisplay($default_view_display);
            $view->preExecute();
            $view->execute();
            $content = $view->buildRenderable();
            $output[] = $content;            
          }
        }
      } else {
      	$view = \Drupal\views\Views::getView('header');
      	$view_display = $default_view_display;
        if (is_object($view)) {
          $view->setDisplay($view_display);
          $view->preExecute();
          $view->execute();
          $content = $view->buildRenderable();
          $output[] = $content;
        }
      }
      
      
    	return $output;
    	}

    }
}
 