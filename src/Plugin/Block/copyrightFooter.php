<?php

namespace Drupal\fuseiq\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
use Drupal\Core\Entity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Copyright' Block.
 *
 * @Block(
 *   id = "fuseiq_copyright",
 *   admin_label = @Translation("Copyright block for footer"),
 * )
 */
class copyrightFooter extends BlockBase {
  

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['copyright_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Copyright Name'),
      '#default_value' => isset($this->configuration['copyright_name']) ? $this->configuration['copyright_name'] : 'FuseIQ',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );
  
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['copyright_name'] = $form_state->getValue('copyright_name');
  }

  /**
   * {@inheritdoc}
   */
     
  public function build() {	 
    
    $year = date('Y',time()); 
	  
	  $output = array(
      '#copyright' => '© ' . $year . ' - ' . $this->configuration['copyright_name'],
      '#theme' => 'copyright_footer',
	  );   
      
    	return $output;

    }
}
 