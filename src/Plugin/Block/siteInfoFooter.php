<?php

namespace Drupal\fuseiq\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
use Drupal\Core\Entity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Site Info' Block.
 *
 * @Block(
 *   id = "fuseiq_site_info",
 *   admin_label = @Translation("Site Info block for footer"),
 * )
 */
class siteInfoFooter extends BlockBase {
  

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['site_info_text'] = array(
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Site Info'),
      '#description' => $this->t('Site information'),
      '#default_value' => isset($this->configuration['site_info_text']) ? $this->configuration['site_info_text'] : '',
      '#weight' => 0,
    );
  
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['site_info_text'] = $form_state->getValue('site_info_text')['value'];
  }

  /**
   * {@inheritdoc}
   */
     
  public function build() {	 
    
    $year = date('Y',time()); 
	  
	  $output = array(
      '#textcontent' => $this->configuration['site_info_text'],
      '#theme' => 'site_info_footer',
	  );   
      
    	return $output;

    }
}
 